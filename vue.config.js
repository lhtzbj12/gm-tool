// const path = require('path')
// function resolve (dir) {
//   return path.join(__dirname, dir)
// }
module.exports = {
  // 打包后时，css js等存放的路径
  assetsDir: 'static', // static
  // 将部署应用程序的基本URL。
  // 默认情况下，Vue CLI假设您的应用程序将部署在域的根目录下。https://www.my-app.com/。
  // 如果应用程序部署在子路径上，则需要使用此选项指定子路径。例如，如果您的应用程序部署在https://www.foobar.com/my-app/，集baseUrl到'/my-app/'.
  // 在Public/index.html里使用 <%= BASE_URL %>
  publicPath: '',
  // 保存时进行eslin检查
  lintOnSave: true,
  // 如果您不需要生产时的源映射，那么将此设置为false可以加速生产构建
  productionSourceMap: false,
  // webpack配置
  // see https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md
  chainWebpack: config => {
    // svg文件处理
    const svgRule = config.module.rule('svg')
    // 清除已有的所有loader
    svgRule.uses.clear()
    svgRule.use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
    // 小图片处理 调整内联文件的大小为10kb
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .tap(options => Object.assign(options, { limit: 10240 }))
  },
  // Webpack配置
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {

    }
  },
  // PWA 插件相关配置
  // see https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa
  pwa: {},
  // webpack-dev-server 相关配置
  devServer: {
    open: process.platform === 'darwin',
    host: '0.0.0.0',
    port: 8000,
    https: false,
    hotOnly: false,
    proxy: {
      '/b': {
        target: 'http://localhost:8011/',
        changeOrigin: true
      }
    }, // 设置代理
    before: app => { },
    disableHostCheck: true // 不进行域名检查
  },
  // 第三方插件配置
  pluginOptions: {
    // ...
  }
}
