import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    step: 1,
    curTableID: '',
    tableInfos: [],
    connectted: false,
    curScheme: { // 当前方案

    }
  },
  getters: {
    tableInfos: (state) => state.tableInfos,
    step: (state) => state.step,
    curTableID: (state) => state.curTableID,
    dbInfo: (state) => state.dbInfo,
    connectted: (state) => state.connectted,
    curScheme: (state) => state.curScheme
  },
  actions: {
    setTableInfos: ({ commit }, value) => commit('SET_TABLE_INFO', value),
    setStep: ({ commit }, value) => commit('SET_STEP', value),
    setCurTableID: ({ commit }, value) => commit('SET_CUR_TABLE_ID', value),
    setConnectted: ({ commit }, value) => commit('SET_CONNECTTED', value),
    setCurScheme: ({ commit }, value) => commit('SET_CUR_SCHEME', value)
  },
  mutations: {
    SET_TABLE_INFO: (state, value) => {
      state.tableInfos = value
    },
    SET_STEP: (state, value) => {
      state.step = value
    },
    SET_CUR_TABLE_ID: (state, value) => {
      state.curTableID = value
    },
    SET_CONNECTTED: (state, value) => {
      state.connectted = value
    },
    SET_CUR_SCHEME: (state, value) => {
      state.curScheme = value
    }
  }
})
