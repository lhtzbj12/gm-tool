/**
 *  对称加密、解密类
 * */
import sm4M from 'miniprogram-sm-crypto'
const sm4 = sm4M.sm4

// 加密
export const sm4Enc = (word, key) => {
  const encryptData = sm4.encrypt(word, key)
  return encryptData
}

// 解密
export const sm4Dec = (word, key) => {
  const decryptData = sm4.decrypt(word, key) // 解密结果
  return decryptData
}

// 生成密钥
export const sm4GenKey = () => {
  return randomStr(16)
    .split('')
    .map(function (c) {
      return c.charCodeAt(0).toString(16)
    })
    .join('')
}

// 生成随机数
function randomStr (len) {
  const x = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789'
  let str = ''
  for (let i = 0; i < len; i++) {
    str += x[~~(Math.random() * x.length)]
  }
  return str
}
