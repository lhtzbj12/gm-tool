import axios from 'axios'
// import router from '@/router'
// axios.defaults.withCredentials = true
// import store from '@/store'
// import { getToken } from '@/utils/auth'

// create an axios instance
const service = axios.create({
  baseURL: '/', // api的base_url
  timeout: 50000 // request timeout
})

// request interceptor
service.interceptors.request.use(config => {
  // Do something before request is sent
  // if (store.getters.token) {
  //   让每个请求携带token-- ['X-Token']为自定义key 请根据实际情况自行修改
  //   config.headers['X-Token'] = getToken()
  // }
  config.headers['If-Modified-Since'] = '0'
  config.headers['Cache-Control'] = 'no-cache'
  config.headers['Pragma'] = 'no-cache'
  config.headers['X-Requested-With'] = 'XMLHttpRequest'
  // config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
  return config
}, error => {
  // Do something with request error
  console.log(error) // for debug
  Promise.reject(error)
})

// respone interceptor
service.interceptors.response.use(
  response => {
    // 这里根据不同的code进行相应处理
    if (response.data && response.data.code === 0) {
      return Promise.resolve(response.data)
    } else {
      return Promise.reject(new Error(`${response.data.msg} ${response.data.data}`))
    }
  },
  error => {
    return Promise.reject(error)
  })

export default service
