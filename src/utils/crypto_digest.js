/**
 *  摘要
 * */
import smCrypto from 'miniprogram-sm-crypto'
const sm3Func = smCrypto.sm3

// 加密
export const sm3 = (word) => {
  const digest = sm3Func(word)
  return digest
}
