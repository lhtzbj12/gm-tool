﻿#!/bin/sh -l
# 下面的内容需要jenkins的环境变量，只能在jenkins里执行
#*********************************************
echo $REGISTRY
echo $APP_NAME
echo $IMAGE_TAG
echo $K8S_NAMESPACE
#**************** 以下内容不用修改 **********************#
# 向k8s执行命令。。。
echo '向k8s执行命令...'
# 替换模板文件里的特殊字符，生成需要提交到k8s的yaml文件
replace_exp="s@##app_name##@$APP_NAME@g"
sed $replace_exp k8s-deployment_templ.yaml | tee k8s-deployment.yaml
# 替换k8s的yaml文件里的镜像名称
replace_exp="s@##image_tag##@$IMAGE_TAG@g"
sed -i $replace_exp k8s-deployment.yaml
# 替换k8s的yaml文件里的命名空间
replace_exp="s@##namespace##@$K8S_NAMESPACE@g"
sed -i $replace_exp k8s-deployment.yaml
cat k8s-deployment.yaml
kubectl apply -f k8s-deployment.yaml

